import React from 'react'
import Layout from '../components/Layout'

const Contact = () => {
    return (
        <Layout>
        <h1>Contact</h1>
        <p>The best way to reach me is via @midexigne on twitter.</p>
    </Layout>
    )
}

export default Contact