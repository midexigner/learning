import { Link } from 'gatsby'
import React from 'react'
import Layout from '../components/Layout'

const index = () => {
  return (
    <Layout>
      <h1>Hello.</h1>
      <h2>I'm Muhammad Idrees, a full-stack developer living in beautiful karachi</h2>
      <p>Need a developer? <Link to="/contact">Contact me.</Link></p>
    </Layout>
  )
}

export default index
