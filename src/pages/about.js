import { Link } from 'gatsby'
import React from 'react'
import Layout from '../components/Layout'

const About = () => {
    return (
        <Layout>
        <h1>About</h1>
        <p>I currently teach full-time on Udemy.</p>
        <p><Link to="/contact">Want to work with me? React out.</Link></p>
    </Layout>
    )
}

export default About
