import React from 'react'
import Layout from '../components/Layout'

const Blog = () => {
    return (
        <Layout>
            <h1>Blog</h1>
            <p>Posts will show up here later on.</p>
        </Layout>
    )
}

export default Blog
