import React from 'react'

const Footer = () => {
    return (
        <footer>
            <p>Created by MI Dexigner, &copy; { new Date().getFullYear()}</p>
        </footer>
    )
}

export default Footer
